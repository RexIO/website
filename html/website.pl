#!/usr/bin/env perl -w

use strict;
use warnings;
use utf8;

use DateTime;

use Cwd qw(getcwd);
use Mojolicious::Lite;
use Mojo::UserAgent;
use Data::Dumper;

plugin 'RenderFile';
plugin 'Config', file => 'config.pl';

get '/' => sub {
   my ($self) = @_;
   $self->stash("no_side_bar", 0);
   $self->render("index", root => 1, cat => "", no_disqus => 1);
};

get '/tour' => sub {
   my ($self) = @_;
   $self->stash("no_side_bar", 0);

   my @images;
   opendir(my $dh, "public/tour");
   while(my $entry = readdir($dh)) {
      next if($entry =~ m/^\./);
      next if(-d $entry);
      next if($entry =~ m/\.thumb\.png$/);
      next if($entry =~ m/\.txt$/);

      my $thumb = $entry;
      $thumb =~ s/\.png$/.thumb.png/;

      my $desc = eval { local(@ARGV, $/) = ("public/tour/$entry.txt"); <>; };
      chomp $desc;

      push @images, {
         name  => $entry,
         thumb => $thumb,
         desc  => $desc,
      };
   }
   closedir($dh);

   @images = sort { [ split /_/, $a->{name} ]->[0] <=> [ split /_/, $b->{name} ]->[0] } @images;

   $self->render("tour", root => 1, cat => "", no_disqus => 1, images => \@images);
};

get '/*file' => sub {
   my ($self) = @_;

   my $template = $self->param("file");

   my ($cat) = ($template =~ m/^([^\/]+)\//);
   $cat ||= "";
   $self->stash("cat", $cat);

   if($template eq "howtos/compatibility.html") {
      $self->stash("no_side_bar", 1);
   }
   else {
      $self->stash("no_side_bar", 0);
   }

   if(-f "public/$template") {
      return $self->render_file(filepath => "public/$template", no_disqus => 0, root => 0);
   }

   if(-d "templates/$template") {
      return $self->redirect_to("$template/index.html", root => 0);
   }

   if(-f "templates/$template.ep") {
      $template =~ s/\.html$//;
      $self->render($template, no_disqus => 0, root => 0);
   }
   else {
      $self->render('404', status => 404, no_disqus => 1, root => 0);
   }

};


app->start;

__DATA__

@@ 404.html.ep

% layout 'default';
% title '404 - File not found';

<h1>404 - I'm really sorry :(</h1>
<p>Hello. I'm a Webserver. And i'm there to serve files for you. But i'm really sorry :( I can't find the file you want to see.</p>

@@ index_head.html.ep

         <div id="head-div">
            <div class="head_container">
               <div class="slogan">
                  <h1>Next Level Datacenter Automation</h1>
                  <div class="slogan_list">
                     <ul>
                        <li>&gt; Easy baremetal deployment</li>
                        <li>&gt; Deploy servers with just a view clicks</li>
                        <li>&gt; From bare metal to production in a coffee break</li>
                        <li>&gt; Apache 2.0 licensed</li>
                     </ul>
                  </div> <!-- slogan_list -->

                  <a class="headlink" href="/tour.html">Take a tour</a>
               </div> <!-- slogan -->

            </div> <!-- head_container -->
         </div>


@@ navigation.html.ep

             <div class="nav_links">
               <ul>
                  <li <% if($cat eq "") { %>class="active" <% } %>><a href="/">Home</a></li>
                  <li <% if($cat eq "get") { %>class="active" <% } %>><a href="/get" title="Install Rex.IO on your systems">Get Rex.IO</a></li>
                  <li <% if($cat eq "contribute") { %>class="active" <% } %>><a href="/contribute" title="Help Rex.IO to get even better">Contribute</a></li>
                  <li <% if($cat eq "howtos") { %>class="active" <% } %>><a href="/howtos" title="Examples, Howtos and Documentation">Docs</a></li>
               </ul>
            </div>

@@ layouts/default.html.ep
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
   <head>
      <meta http-equiv="content-type" content="text/html; charset=utf-8" />
   
      <title><%= title %></title>

      <meta name="viewport" content="width=1024, initial-scale=0.5">

   <script type="text/javascript" charset="utf-8" src="/js/jquery.js"></script>

      <link href="http://yandex.st/highlightjs/7.3/styles/magula.min.css" rel="stylesheet"/>
      <link rel="stylesheet" href="/css/bootstrap.min.css?20130325" type="text/css" media="screen" charset="utf-8" />
      <link rel="stylesheet" href="/css/default.css?20130325" type="text/css" media="screen" charset="utf-8" />
      <link rel="stylesheet" href="/js/jquery.fancybox.css" type="text/css" media="screen" charset="utf-8" />

      <meta name="description" content="Rex.IO - Opensource datacenter automation. Deploy and manage all your servers in your datacenter." />
      <meta name="keywords" content="Systemadministration, Datacenter, Automation, Rex.IO, Remote, Configuration, Management, Framework, SSH, Linux" />

      <style>
      % if($no_side_bar) {
         #content_container {
            margin-left: 5px;
         }

         #widgets {
            display: none;
         }
      % }
      </style>

   </head>
   <body>

      <div id="page">

      % if($root) {
         <div id="index-top-div" class="top_div">
            <h1>Rex.IO <small>Datacenter Automation</small></h1>
         </div>

         %= include 'index_head';
         <div class="nav_title">
            <span style="color: #7b4d29;">Y</span>our <span style="color: #7b4d29;">W</span>ay
         </div>

         <div id="index-nav-div">
            %= include 'navigation';
         </div>
      % } else {
         <div id="top-div" class="top_div">
            <h1>Rex.IO <small>Datacenter Automation</small></h1>
            <div id="nav-div">
               %= include 'navigation';
            </div>
         </div>
      % }

         <div id="widgets_container">
            <div id="widgets">
<!--               <h2>Search</h2>
               <form action="/search" method="GET">
                  <input type="text" name="q" id="q" class="search_field" /><button class="btn">Go</button>
               </form> -->
               <h2>News</h2>

               <div class="news_widget">
                  <div class="news_date">2013-06-19</div>
                  <div class="news_content">Website is online. First preview release of Rex.IO is available now.</div>
               </div>

               <h2>Conferences</h2>
               <img src="/img/linuxtag.jpg" style="float: left; width: 100px" />
               <p>We are proud to anounce that Rex.IO will have a booth on the <a href="http://linuxtag.de/">LinuxTag</a>. The LinuxTag will take place in Berlin from 22. - 25. May. LinuxTag is the most popuplar and important conference in the Linux and Open Source scene in Europe.</p>
               <p>Come and join us, we will do live demostrations with a custom build portable mini datacenter.</p><p>If you want to help us for a day or two don't hesitate to contact <a href="mailto:jfried@rexify.org">@jfried83</a>.</p>

               <h2>Need Help?</h2>
               <p>Rex.IO is a pure Open Source project. So you can find community support in the following places.</p>
               <ul>
                  <li>IRC: #rex on freenode</li>
                  <li>Groups: <a href="https://groups.google.com/group/rex-users/">rex-users</a></li>
                  <li>Issues: <a href="https://github.com/RexIO/RexIO/issues">on Github</a></li>
                  <li>Feature: You miss a <a href="https://github.com/RexIO/RexIO/issues">feature</a>?</li>
               </ul>
            </div>
         </div> <!-- widgets -->

         <div id="content_container">
            <div id="content">
               <%= content %>

 % if( ! $no_disqus) {

    <div id="disqus_thread" style="margin-top: 45px;"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
        var disqus_shortname = 'rexify'; // required: replace example with your forum shortname

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
    <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
    
    % }

            </div>
         </div> <!-- content -->

      </div> <!-- page -->
      

     <a href="http://github.com/RexIO"><img style="position: absolute; top: 0; right: 0; border: 0;" src="https://s3.amazonaws.com/github/ribbons/forkme_right_orange_ff7600.png" alt="Fork me on GitHub" /></a>
     <img style="position: absolute; top: 0; left: 0; border: 0;" src="/img/preview-badge.png" />



   <script type="text/javascript" charset="utf-8" src="/js/bootstrap.min.js"></script>
   <script type="text/javascript" charset="utf-8" src="/js/jquery.mousewheel-3.0.6.pack.js"></script>
   <script type="text/javascript" charset="utf-8" src="/js/jquery.fancybox.js"></script>

<!-- Piwik --> 
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://rexify.org/stats/" : "http://rexify.org/stats/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 2);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://rexify.org/stats/piwik.php?idsite=2" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Tracking Code -->

<script src="http://yandex.st/highlightjs/7.3/highlight.min.js"></script>
<script>
  hljs.tabReplace = '    ';
  hljs.initHighlightingOnLoad();
  $("#q").focus();
</script>


   </body>

</html>

